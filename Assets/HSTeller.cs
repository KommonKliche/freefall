﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HSTeller : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Text text = GetComponent<Text>();
        text.text = "Highscore:";

        if (SaveSystem.highscoreData() == null)
        {
            //if Highscore is empty, create default highscore   
            ClearHS();
        }

        text.text = text.text + "\n\t" + SaveSystem.highscoreData().highScore.ToString() + "\n\t\t" + SaveSystem.highscoreData().playerName.ToString();
    }

    public void ClearHS() {
        SaveSystem.SaveScore(50, "LNF");
    }
}
