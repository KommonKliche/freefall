﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class SpeedIndicator : MonoBehaviour
{
    private Text text;

    private void Awake()
    {
        text = gameObject.GetComponent<Text>();
    }
    // Update is called once per frame
    void Update()
    {
        if (text != null) {
            text.text = (Math.Round(LevelManager.S.currentSpeed * 60.0f * 60.0f / 1000.0f, 1) + " kph");
        }
    }
}
