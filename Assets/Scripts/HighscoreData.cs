﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class HighscoreData
{
    static int characterLimit = 3;
    public int highScore = 100;
    public string playerName = "AAA";

    public HighscoreData(int highScore, string playerName) {
        this.highScore = highScore;

        playerName.ToUpper();

        string nameEntry = "";

        if (playerName.Length > characterLimit)
        {

            for (int i = 0; (i < characterLimit || i < playerName.Length); i++)
            {
                nameEntry = nameEntry + playerName[i];
            }
        }
        else {
            nameEntry = playerName;
        }

        this.playerName = nameEntry;
    }
}
