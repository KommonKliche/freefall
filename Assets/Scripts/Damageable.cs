﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Damageable : MonoBehaviour
{
    public int maxHealth;
    private int currentHealth;
    [SerializeField]
    private float recoveryTime = 0.0f; //How long this guy's invincibility lasts
    private float recoveryFrames = 0.0f; // How long this guy will be invincible for
    [SerializeField]
    private bool isLootDropper = false; // Does this have a LootDropper?

    public int CurrentHealth {
        get { return currentHealth; }
    }

    public float Recovery {
        get { return recoveryFrames; }
    }

    // Start is called before the first frame update
    void Start()
    {
        currentHealth = maxHealth;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (recoveryFrames > 0.0f)
        {
            recoveryFrames -= Time.deltaTime;

            if (gameObject.GetComponent<SpriteRenderer>() != null) {
                gameObject.GetComponent<SpriteRenderer>().color = new Color(gameObject.GetComponent<SpriteRenderer>().color.r, gameObject.GetComponent<SpriteRenderer>().color.g, gameObject.GetComponent<SpriteRenderer>().color.b, 0.75f);
            }

            if (recoveryFrames <= 0.1f) {
                recoveryFrames = 0.0f;
                if (gameObject.GetComponent<SpriteRenderer>() != null)
                {
                    gameObject.GetComponent<SpriteRenderer>().color = new Color(gameObject.GetComponent<SpriteRenderer>().color.r, gameObject.GetComponent<SpriteRenderer>().color.g, gameObject.GetComponent<SpriteRenderer>().color.b, 1.0f);
                }
            }
        }
    }

    public void SetInvincibility() {
        recoveryFrames = recoveryTime;
    }

    // Update is called once per frame
    public void TakeDamage(int damage)
    {
        currentHealth -= damage;
        Debug.Log(gameObject.name + " took damage: " + currentHealth + " / " + maxHealth);
        if (currentHealth <= 0)
        {
            if (gameObject.tag == "Enemy") {
                if (ScoreKeeper.S != null)
                {
                    ScoreKeeper.S.AddHits();
                }
            }
            if (isLootDropper)
            {
                LootDropper drp = GetComponent<LootDropper>();
                if (drp != null)
                {
                    drp.rollDrop();
                }
            }
            Destroy(gameObject);
        }
    }

    public void GainHealth(int hpGain)
    {
        currentHealth += hpGain;
        if (currentHealth > maxHealth)
        {
            currentHealth = maxHealth;
        }
    }
}
