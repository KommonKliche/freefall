﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LifeCounter : MonoBehaviour
{

    private int lifeTotal = 5;
    [SerializeField]
    private Image lifePrefab;
    private List<Image> lifeImage;
    [SerializeField]
    private List<Image> parachuteImage;

    GameObject player;
    Damageable playerHealth;

    private void Awake()
    {
        player = GameObject.Find("Player");
        playerHealth = player.GetComponent<Damageable>();
        lifeImage = new List<Image>();
        for (int i = 0; i < player.GetComponent<Damageable>().maxHealth; i++) {
            Image newLife = Instantiate(lifePrefab.gameObject).GetComponent<Image>();
            newLife.rectTransform.parent = gameObject.GetComponent<RectTransform>();
            newLife.rectTransform.localScale = new Vector2(1, 1);
            Rect lifesize = newLife.rectTransform.rect;
            lifesize.width = 60;
            lifesize.height = 60;
            newLife.rectTransform.localPosition = new Vector2(0, 144 - (i * 72));
            lifeImage.Add(newLife);
        }
    }

    private void Update()
    {
        if (player != null)
        {
            if (playerHealth != null)
            {
                for (int i = 0; i < lifeImage.Count; i++)
                {
                    if (playerHealth.CurrentHealth > i)
                    {
                        lifeImage[i].gameObject.SetActive(true);
                    }
                    else
                    {
                        lifeImage[i].gameObject.SetActive(false);
                    }
                }
            }
        }
        else {
            for (int i = 0; i < 5; i++)
            {
                 lifeImage[i].gameObject.SetActive(false);
            }
        }

        if (LevelManager.S.parachute != null && !LevelManager.S.parachuteReleased) {
            LevelManager.S.parachuteReleased = true;
        }

        if (LevelManager.S.parachute != null && LevelManager.S.parachuteReleased)
        {
            Damageable parachuteHealth = LevelManager.S.parachute.GetComponent<Damageable>();

            if (parachuteHealth != null)
            {
                for (int i = 0; i < 3; i++)
                {
                    if (parachuteHealth.CurrentHealth > i)
                    {
                        parachuteImage[i].gameObject.SetActive(true);
                    }
                    else
                    {
                        parachuteImage[i].gameObject.SetActive(false);
                    }
                }
            }
        }

        else if (LevelManager.S.parachute == null && LevelManager.S.parachuteReleased)
        {
            for (int i = 0; i < 3; i++)
            {

                parachuteImage[i].gameObject.SetActive(false);

            }
        }
    }
}
