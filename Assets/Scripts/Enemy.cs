﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    protected Coroutine enemyBehavior; //Enemy AI
    [SerializeField]
    protected float movementSpeed = 5.0f; // speed enemy executes functions at
    [SerializeField]
    protected float shootingRate = 2.5f; //shoootis every s seconds
    protected float timeToShoot = 0.0f;
    protected bool limitY = false;
    [SerializeField]
    protected int node = -1;
    [SerializeField]
    protected int releaseTime = 0;
    protected bool added;
    [SerializeField]
    protected float xBound = 5.0f, yBound = 4.8f;
    protected Rigidbody2D rb;

    [SerializeField]
    protected GameObject gun, arm; //gun and arm obj

    public int Node {
        get {
            if (node > 9 || node < 0) {
                return Random.Range(0,10);
            }
            return node;
        }
    }

    // Start is called before the first frame update
    virtual protected void Start()
    {
        ScoreKeeper.S.AddEnemy();

        added = false;
        GoToSpawn();
        //Initiates Enemy AI
        rb = gameObject.GetComponent<Rigidbody2D>();

        enemyBehavior = StartCoroutine(EnemyBehavior());

        Activate(false);
        
    }

    // Update is called once per frame
    protected virtual void Update()
    {
        
        if (GameObject.FindGameObjectWithTag("Player") == null)
        {
            //EXIT
            if (enemyBehavior != null)
            {
                StopCoroutine(enemyBehavior);
            }

            enemyBehavior = StartCoroutine(Exit());
            return;
        }

        if (transform.position.x - GameObject.FindGameObjectWithTag("Player").transform.position.x < 0)
        {
            //transform.localScale = new Vector2(1,1); // face Right
            GetComponent<SpriteRenderer>().flipX = false;
        }
        else {
            //transform.localScale = new Vector2(-1, 1); // face left
            GetComponent<SpriteRenderer>().flipX = true;
        }

        Aim();
        stayInBounds();
        FlipCheck();
    }

    protected void FlipCheck() {
        if (GetComponent<SpriteRenderer>().flipX == true)
        {
            arm.GetComponentInChildren<SpriteRenderer>().flipX = true;
            gun.GetComponentInChildren<SpriteRenderer>().flipY = true;
        }
        else {
            arm.GetComponentInChildren<SpriteRenderer>().flipX = false;
            gun.GetComponentInChildren<SpriteRenderer>().flipY = false;

        }
    }

    protected virtual void GoToSpawn() {
        transform.position = new Vector2 (LevelManager.S.Nodes[Node].transform.position.x, LevelManager.S.Nodes[Node].transform.position.y);
        //gameObject.SetActive(false);
    }

    protected void Activate(bool active) {
        gun.SetActive(active);
        arm.SetActive(active);
        gameObject.GetComponent<SpriteRenderer>().enabled = active;
        gameObject.GetComponent<BoxCollider2D>().enabled = active;
    }

    protected virtual void Aim() {
        //Shoot Straight
        if (GameObject.FindGameObjectWithTag("Player") == null || !arm.gameObject.activeInHierarchy)
        {
            return;
        }

        timeToShoot += Time.deltaTime; //set Up to shoot at frequency 

        GameObject player = GameObject.FindGameObjectWithTag("Player");

        if (player.transform.position.x > transform.position.x)
        {
            //aim right
            arm.transform.eulerAngles = new Vector3(0, 0, -90);
        }
        else {
            //aim left
            arm.transform.eulerAngles = new Vector3(0, 0, 90);
        }

        if (timeToShoot >= shootingRate) {
            gun.GetComponent<Gun>().Shoot();
            timeToShoot = 0.0f; // reset timer to shoot
        }
    }

    protected void stayInBounds() {
        if (transform.position.x > xBound) {
            transform.position = new Vector2(xBound, transform.position.y);
        } else if (transform.position.x < -xBound)
        {
            transform.position = new Vector2(-xBound, transform.position.y);
        }

        if (limitY) {
            if (transform.position.y > yBound)
            {
                transform.position = new Vector2(transform.position.x , yBound);
            }
            else if (transform.position.y < -yBound)
            {
                transform.position = new Vector2(transform.position.x, -yBound);
            }
        }

    }

    virtual protected IEnumerator EnemyBehavior() {

        //has enemy wait
        while (!added)
        {
            if ((int)LevelManager.S.currentTime >= releaseTime + 5)
            {
                /*if (ScoreKeeper.S != null)
                {
                    ScoreKeeper.S.AddEnemy();
                }*/
                added = true;
                Activate(true);
            }
            yield return new WaitForSeconds(1 / 60.0f);
        }

        for (int i = 0; i < 3; i++) {
            if (GameObject.FindGameObjectWithTag("Player") == null) {
                //EXIT
                
                enemyBehavior = StartCoroutine(Exit());
                yield break;
            }

            limitY = true;

            while (Mathf.Abs(GameObject.FindGameObjectWithTag("Player").transform.position.y - transform.position.y) > 0.5f) {
                if (GameObject.FindGameObjectWithTag("Player").transform.position.y < transform.position.y)
                {
                    rb.AddForce(new Vector2(0, -movementSpeed));
                }
                else {
                    rb.AddForce(new Vector2(0, movementSpeed));
                }
                yield return new WaitForSeconds(1 / 60.0f);
            }

            yield return new WaitForSeconds(0.5f);

            if (GameObject.FindGameObjectWithTag("Player") == null)
            {
                //EXIT
                enemyBehavior = StartCoroutine(Exit());
                yield break;
            }

            Debug.Log("Enemy Shooting"); //Insert Shoot Here
            
        }
        
        enemyBehavior = StartCoroutine(Exit());
        yield break;
    }

    protected IEnumerator Exit() {
        //Zoom up the screen
        Debug.Log("EXIT: " + name);
        limitY = false;
        while (transform.position.y < 6) {
            rb.AddForce(new Vector2(0, movementSpeed*2));
            yield return new WaitForSeconds(1 / 60.0f);
        }
        //Add Miss
        if (ScoreKeeper.S != null)
        {
            ScoreKeeper.S.AddMiss();
        }
        Destroy(gameObject);

        yield break;
    }

    
}
