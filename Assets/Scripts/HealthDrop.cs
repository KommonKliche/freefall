﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthDrop : MonoBehaviour
{
    public int hpGain;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            Damageable player = other.gameObject.GetComponent<Damageable>();

            player.GainHealth(hpGain);

            Destroy(gameObject);
        }
    }
}