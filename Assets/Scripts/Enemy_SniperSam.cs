﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_SniperSam : Enemy
{
    private Vector2 origin;
    public static float shootHeight = 3.5f;
    protected override void GoToSpawn()
    {
         
        origin = new Vector2(LevelManager.S.Nodes[Node].transform.position.x, LevelManager.S.Nodes[Node].transform.position.y);

        transform.position = origin;
    }

    protected override void Aim()
    {
        if (GameObject.FindGameObjectWithTag("Player") == null || !arm.gameObject.activeInHierarchy)
        {
            return;
        }

        GameObject player = GameObject.FindGameObjectWithTag("Player");

        if (player.transform.position.y > transform.position.y)
        {
            //aim high
            arm.transform.eulerAngles = new Vector3(0, 0, 0);
        }
        else
        {
            //aim low
            arm.transform.eulerAngles = new Vector3(0, 0, 180);
        }
    }


    protected override IEnumerator EnemyBehavior()
    {
        while (!added)
        {
            if ((int)LevelManager.S.currentTime >= releaseTime + 5)
            {
                /*if (ScoreKeeper.S != null)
                {
                    ScoreKeeper.S.AddEnemy();
                }*/
                added = true;
                Activate(true);
            }
            yield return new WaitForSeconds(1 / 60.0f);
        }

        if (GameObject.FindGameObjectWithTag("Player") == null)
        {
            enemyBehavior = StartCoroutine(Exit());
            yield break;
        }

        GameObject player = GameObject.FindGameObjectWithTag("Player");

        //Enter Screen
        if (origin.y < 0)
        {
            //from bottom
            while ((transform.position.y) < (-shootHeight))
            {
                rb.AddForce(new Vector2(0.0f, movementSpeed));
                yield return new WaitForSeconds(1 / 60.0f);
            }
        }
        else {
            //from top

            while ((transform.position.y) > (shootHeight))
            {
                rb.AddForce(new Vector2(0.0f, -movementSpeed));
                yield return new WaitForSeconds(1 / 60.0f);
            }
        }

        rb.velocity = Vector2.zero; // stop

        limitY = true;

        //Shooting Routine
        for (int i = 1; i <= shootingRate; i++) {

            if (GameObject.FindGameObjectWithTag("Player") == null)
            {
                enemyBehavior = StartCoroutine(Exit());
                yield break;
            }

            //travel to player X
            while (Mathf.Abs(player.transform.position.x - transform.position.x) > 0.5f) {
                if (player.transform.position.x > transform.position.x)
                {
                    //move right
                    rb.AddForce(new Vector2(movementSpeed, 0));
                }
                else {
                    //left
                    rb.AddForce(new Vector2(-movementSpeed, 0));
                }

                yield return new WaitForSeconds(1/60.0f);
            }

            //shoot twice
            gun.GetComponent<Gun>().Shoot();
            yield return new WaitForSeconds(0.15f);

            gun.GetComponent<Gun>().Shoot();
            yield return new WaitForSeconds(0.15f);

            //wait
            yield return new WaitForSeconds(1.5f);
        }

        enemyBehavior = StartCoroutine(Exit());
        yield break;

    }
}
