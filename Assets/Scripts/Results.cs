﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Results : MonoBehaviour
{
    public static Results S;

    public Text text;

    public InputField field;

    public GameObject exitButton;

    private Coroutine coroutine;

    private int scoreSend = 0;

    private void Awake()
    {
        if (S == null)
        {
            S = this;
        }
        else {
            Destroy(this.gameObject);
        }
        field.gameObject.SetActive(false);
        exitButton.gameObject.SetActive(false);
        gameObject.SetActive(false);

        scoreSend = 0;
    }

    public void ShowResults() {
        if (coroutine == null) {
            coroutine = StartCoroutine(DisplayResults());
        }
    }

    public IEnumerator DisplayResults() {
        int hits = 0;
        int miss = 0;
        int score = 0;
        bool noDamage = false;
        while (hits < ScoreKeeper.S.Hits) {
            hits++;
            text.text = "Hits:\t\t\t" + hits + "\nMiss:\t\t\t" + miss + "\nScore:\t\t\t" + score;
            yield return new WaitForSeconds(1 / 30.0f);
        }

        while (miss < ScoreKeeper.S.Misses)
        {
            miss++;
            text.text = "Hits:\t\t\t" + hits + "\nMiss:\t\t\t" + miss + "\nScore:\t\t\t" + score;
            yield return new WaitForSeconds(1 / 30.0f);
        }

        while (score < ScoreKeeper.S.Results)
        {
            score++;
            text.text = "Hits:\t\t\t" + hits + "\nMiss:\t\t\t" + miss + "\nScore:\t\t\t" + score;
            yield return new WaitForSeconds(1 / 15.0f);
        }

        score = ScoreKeeper.S.Results;

        text.text = "Hits:\t\t\t" + hits + "\nMiss:\t\t\t" + miss + "\nScore:\t\t\t" + score;

        yield return new WaitForSeconds(1);

        if (FindObjectOfType<PlayerController>() != null) {

            if (FindObjectOfType<PlayerController>().gameObject.GetComponent<Damageable>().CurrentHealth ==
            FindObjectOfType<PlayerController>().gameObject.GetComponent<Damageable>().maxHealth) {
                score += 20;
                text.text = "Hits:\t\t\t" + hits + "\nMiss:\t\t\t" + miss + "\nScore:\t\t\t" + score;
                text.text = text.text + "\nNo Damage!";
            }
        }

        scoreSend = score;

        if (SaveSystem.highscoreData() != null)
        {
            if (SaveSystem.highscoreData().highScore < score)
            {
                text.text = text.text + "\nNew High Score!!\nEnter Name>>";

                //open textbox
                field.gameObject.SetActive(true);
                //take name

                //save data
            }
        }

        yield return new WaitForSeconds(2);
        //Make exit button appear
        exitButton.SetActive(true);
        yield break;
    }

    public void ExitLevel() {

        if (field.IsActive()) {
            string entry = "";
            if (field.text == "")
            {
                entry = "???";
            }
            else {
                entry = field.text;
            }
            SaveSystem.SaveScore(scoreSend, entry);
        }

        SceneManager.LoadScene(0);
    }

}
