﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Indicator : MonoBehaviour
{
    [SerializeField]
    private Image gauge, arrow;

    private void Awake()
    {
        arrow.rectTransform.localPosition = new Vector2( arrow.rectTransform.localPosition.x, gauge.rectTransform.rect.height / 2);
    }

    // Update is called once per frame
    void Update()
    {
        
        if (LevelManager.S != null) {

            int progress = (int) (((LevelManager.S.currentTime / LevelManager.S.LevelTime)) * (gauge.rectTransform.rect.height));
            arrow.rectTransform.localPosition = new Vector2(arrow.rectTransform.localPosition.x, gauge.rectTransform.rect.height / 2 - progress);
        }
    }
}
