﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour
{

    // Set in Inspector
    public GameObject bullet; // Bullet object fired by this gun
    public float fireRate = 5; // Delay between shots. Higher values shoot slower
    public float bulletSpeed = 1; // Speed bullet travels at
    public int bulletDamage = 1; // Damage bullets deal
    public int maxBullets = 1; // Maximum amount of bullets that can be on screen at any given time;

    // Do not set manually
    protected float nextShot; // Minimum value of elapsed time before then next shot can be fired
    protected GameObject[] bullets;
    protected AudioSource shotSound;

    // Start is called before the first frame update
    protected void Start()
    {
        nextShot = 0; // Initialize nextShot
        bullets = new GameObject[maxBullets]; // Initialize array to store bullets in
        for (int b = 0; b < maxBullets; b++) // Instantiate bullets in array
        {
            GameObject bull = Instantiate(bullet, transform.position, transform.rotation); // Instantiate bullet
            bull.tag = gameObject.tag;
            bullets[b] = bull; // Assign bullet to array
            bull.SetActive(false); // Disable bullet to hide it
            if (tag == "Player")
            {
                bull.layer = 10;
            }// Check if it's a player's bullet and set the layer accordingly
            else {
                bull.layer = 11;
            } // or set it for enemies
        }

        shotSound = GetComponent<AudioSource>();
    }

    public virtual void Shoot()
    {
        if (Time.time >= nextShot)
        {
            foreach (GameObject bull in bullets) // Search through all of the bullets
            {
                if (bull.activeSelf == false) // If it hasn't been fired yet...
                {
                    shotSound.Play();
                    bull.GetComponent<Bullet>().Fire(bulletSpeed, bulletDamage, transform, 0, GetComponentInChildren<SpriteRenderer>().flipY); // Call the bullet's Fire method
                    nextShot = Time.time + (1 / fireRate); // Set the timer for the next shot
                    break; // Leave the loop until the next time the player shoots
                }
            }
        }
    }

    public virtual void Shoot(float offset = 0) {
        if (Time.time >= nextShot)
        {
            foreach (GameObject bull in bullets) // Search through all of the bullets
            {
                if (bull.activeSelf == false) // If it hasn't been fired yet...
                {
                    bull.GetComponent<Bullet>().Fire(bulletSpeed, bulletDamage, transform, offset, GetComponentInChildren<SpriteRenderer>().flipY); // Call the bullet's Fire method
                    nextShot = Time.time + (1 / fireRate); // Set the timer for the next shot
                    break; // Leave the loop until the next time the player shoots
                }
            }
        }
    }
}
