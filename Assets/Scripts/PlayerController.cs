﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float xBounds;
    public float yBounds;
    public bool controllable = true;
    public Vector3 armOffset;

    public float moveSpeed = 0.3f;
    public GameObject armObject;

    public AudioClip gunEquipSound;

    public List<GameObject> Guns = new List<GameObject>(); // Stores our player's gun selection

    private Rigidbody2D rb;
    private GameObject currentGun;
    private float aimEuler = 0;
    private GameObject arm;
    [SerializeField]
    public GameObject parachutePrefab;
    public GameObject parachute;
    private bool parachuteDeployable = true;
    private AudioSource audioPlayer;

    public bool ParachuteDeployed {
        get { if (parachute != null)
            {
                return true;
            }
            else {
                return false;
            }
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        arm = Instantiate(armObject, transform);
        arm.tag = gameObject.tag;
        arm.transform.position += armOffset;
        rb = gameObject.GetComponent<Rigidbody2D>();
        currentGun = Instantiate(Guns[0], arm.transform);
        currentGun.tag = "Player";
        parachuteDeployable = true;
        controllable = true;
        audioPlayer = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!controllable) {
            return;
        }

        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");
        float inputX = Input.GetAxis("Aim Vertical");
        float inputY = Input.GetAxis("Aim Horizontal");
        Vector2 aim = new Vector2(inputX, inputY);

        /*if (Input.GetButtonDown("Fire1") && parachute == null && parachutePrefab != null && parachuteDeployable) {
            parachute = GameObject.Instantiate(parachutePrefab, gameObject.transform);
            parachuteDeployable = false;
        }*/

        // X-Axis Boundaries
        if (transform.position.x <= -xBounds)
        {
            transform.position = new Vector2(-xBounds, transform.position.y);
        }
        else if (transform.position.x >= xBounds)
        {
            transform.position = new Vector2(xBounds, transform.position.y);
        }

        // Y-Axis Boundaries
        if (transform.position.y <= -yBounds)
        {
            transform.position = new Vector2(transform.position.x, -yBounds);
        }
        else if (transform.position.y >= yBounds)
        {
            transform.position = new Vector2(transform.position.x, yBounds);
        }

        rb.AddForce(new Vector2(horizontal, vertical) * moveSpeed * Time.deltaTime);

        if (currentGun.activeSelf == false)
        {
            currentGun.SetActive(true);
        }

        // aimEuler = getAim();
        if (inputX != 0 && inputY != 0)
        {
            if (aim.x == -1 && aim.y == 1)
            {
                aimEuler = 225;
            }

            if (aim.x == 1 && aim.y == 1)
            {
                aimEuler = 315;
            }

            if (aim.x == -1 && aim.y == -1)
            {
                aimEuler = 135;
            }

            if (aim.x == 1 && aim.y == -1)
            {
                aimEuler = 45;
            }
        }

        else
        {
            if (aim.x == -1)
            {
                aimEuler = 180;
            }

            if (aim.x == 1)
            {
                aimEuler = 0;
            }

            if (aim.y == 1)
            {
                aimEuler = 270;
            }

            if (aim.y == -1)
            {
                aimEuler = 90;
            }
        }

        arm.transform.localEulerAngles = new Vector3 (0,0,aimEuler);

        if (Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.Keypad8)) // Player is aiming and delay is passed
        {
            currentGun.GetComponent<Gun>().Shoot(); // Fire a bullet
        }
    }


    private void SwitchGun(GameObject newGun)
    {
        // verify gun is available
        // disable currentGun
        // currentGun = newGun
        // enable currentGun
    }

    public void End(bool parachute) {
        arm.SetActive(false);
        Animator animator = gameObject.GetComponent<Animator>();

        animator.SetTrigger("End");

        animator.SetBool("Parachute", parachute);
    }

    public void EquipGun(Gun newGun) {
        Destroy(currentGun.gameObject);
        audioPlayer.Play();
        currentGun = Instantiate(newGun.gameObject, arm.transform);
        currentGun.tag = "Player";
        Debug.Log("Gun Changed");
    }

    /*private float getAim()
    {
        float euler;
        float inputX = Input.GetAxis("Aim Vertical");
        float inputY = Input.GetAxis("Aim Horizontal");
        Vector2 aim = new Vector2(inputX, inputY);

        if (inputX != 0 && inputY != 0)
        {
            if (aim.x == -1 && aim.y == 1)
            {
                euler = 315;
            }

            if (aim.x == 1 && aim.y == 1)
            {
                euler = 45;
            }

            if (aim.x == -1 && aim.y == -1)
            {
                euler = 225;
            }

            if (aim.x == 1 && aim.y == -1)
            {
                euler = 135;
            }
        }

        else
        {
            if (aim.x == -1)
            {
                euler = 270;
            }

            if (aim.x == 1)
            {
                euler = 90;
            }

            if (aim.y == 1)
            {
                euler = 0;
            }

            if (aim.y == -1)
            {
                euler = 180;
            }
        }

        return euler;
    }*/
}
