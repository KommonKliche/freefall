﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_ParachuteDan : Enemy
{
    [SerializeField]
    private float startingLocation = 0.0f;
    [SerializeField]
    private float widthOfSwing = 1;
    private float timeActive = 0.0f;

    // Start is called before the first frame update
    protected override void Start()
    {
        ScoreKeeper.S.AddEnemy();
        added = false;
        GoToSpawn();

        if (transform.position.x >= 0.0f)
        {
            startingLocation = Mathf.Sqrt(10.0f);
        }
        else {
            startingLocation = Mathf.Sqrt(10.0f) * -1.0f;
        }

        transform.position = new Vector2(startingLocation * widthOfSwing, 5.0f);
        rb = gameObject.GetComponent<Rigidbody2D>();
        enemyBehavior = StartCoroutine(EnemyBehavior());

        Activate(false);
    }



    protected override void Aim()
    {
        //Shoot Straight
        if (GameObject.FindGameObjectWithTag("Player") == null || !arm.gameObject.activeInHierarchy)
        {
            return;
        }

        timeToShoot += Time.deltaTime; //set Up to shoot at frequency 

        GameObject player = GameObject.FindGameObjectWithTag("Player");

        float shotAngle = -90;

        //Get hypotenuse
        float measureDistance = Mathf.Sqrt(Mathf.Pow( (player.transform.position.y - transform.position.y), 2) + Mathf.Pow((player.transform.position.x - transform.position.x), 2));


        shotAngle += (Mathf.Asin((player.transform.position.y - transform.position.y) / measureDistance)/(2*Mathf.PI))*360.0f;

        if (GetComponent<SpriteRenderer>().flipX)
        {
            shotAngle += 180 - ((Mathf.Asin((player.transform.position.y - transform.position.y) / measureDistance) / (2 * Mathf.PI)) * 360.0f) * 2.0f;
        }

        arm.transform.eulerAngles = new Vector3(0, 0, shotAngle);

        if (timeToShoot >= shootingRate)
        {
            gun.GetComponent<Gun>().Shoot();
            timeToShoot = 0.0f; // reset timer to shoot
        }
    }

    protected override IEnumerator EnemyBehavior()
    {
        while (!added)
        {
            if ((int)LevelManager.S.currentTime >= releaseTime + 5)
            {
                /*if (ScoreKeeper.S != null)
                {
                    ScoreKeeper.S.AddEnemy();
                }*/
                added = true;
                Activate(true);
            }
            yield return new WaitForSeconds(1 / 60.0f);
        }

        float swingDirection = 1.0f;

        if (startingLocation > 0) {
            swingDirection = -1;
        }

        float stopTime = ((Mathf.Sqrt(10.0f) + Mathf.Sqrt(9.5f) ) / movementSpeed) ;

        //limitY = true;
        timeActive = 0;
        while ( timeActive < stopTime) {
            float nextPosition = (transform.position.x/widthOfSwing) + (1/60.0f)*movementSpeed*swingDirection;
            transform.position = new Vector2(nextPosition * widthOfSwing, ((Mathf.Pow(nextPosition,2))-4.5f));
            timeActive += 1 / 60.0f;
            yield return new WaitForSeconds(1 / 60.0f);
        }
        enemyBehavior = StartCoroutine(Exit());
        yield break;
    }

}
