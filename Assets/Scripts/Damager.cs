﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Damager : MonoBehaviour
{
    public int damage;
    public bool dieOnContact = false;

    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.gameObject.tag == gameObject.tag)
        {
            return;
        }

        Damageable damaged = collision.gameObject.GetComponent<Damageable>();
        if (damaged != null)
        {
            if (damaged.Recovery > 0.0f)
            {
                return;
            }

            damaged.TakeDamage(damage); //deal damage
            damaged.SetInvincibility(); //sets invincibility frames

            if (gameObject.GetComponent<Bullet>() != null)
            {
                this.gameObject.SetActive(false);
            }

            if (dieOnContact)
            {
                Destroy(gameObject);
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == gameObject.tag) {
            return;
        }

        Damageable damaged = collision.gameObject.GetComponent<Damageable>();
        if (damaged != null)
        {
            if (damaged.Recovery > 0.0f) {
                return;
            }

            damaged.TakeDamage(damage); //deal damage
            damaged.SetInvincibility(); //sets invincibility frames

            if (gameObject.GetComponent<Bullet>() != null) {
                this.gameObject.SetActive(false);
            }

            if (dieOnContact)
            {
                Destroy(gameObject);
            }
        }
    }
}
