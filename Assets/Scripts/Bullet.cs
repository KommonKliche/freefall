﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float maxLifetime = 1; // Distance in Unity units that the bullet will travel before despawning

    Rigidbody2D rb;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        float perfectX = Mathf.Abs(transform.position.x);
        float perfectY = Mathf.Abs(transform.position.y);

        if (Mathf.Abs(transform.position.x) > 5.5f || Mathf.Abs(transform.position.y) > 5) {
            gameObject.SetActive(false);
            rb.velocity = Vector2.zero;
            rb.angularVelocity = 0;
        }

        if (perfectX > maxLifetime || perfectY > maxLifetime)
        {
            gameObject.SetActive(false);
            rb.velocity = Vector2.zero;
            rb.angularVelocity = 0;
            // Destroy(gameObject);
        }
    }

    public void Fire(float speed, int damage, Transform parent)
    {
        transform.SetParent(parent);
        transform.localPosition = new Vector2(0.3f, 0.12f);
        //transform.position = parent.position;
        transform.rotation = parent.rotation;
        gameObject.tag = parent.tag;
        gameObject.SetActive(true);
        Rigidbody2D rb = GetComponent<Rigidbody2D>();
        rb.AddRelativeForce(Vector2.right * speed * 100);
        GetComponent<Damager>().damage = damage;
        transform.SetParent(null);
    }

    public void Fire(float speed, int damage, Transform parent, float offset = 0, bool distort = false)
    {
        transform.SetParent(parent);

        if (distort)
        {
            transform.localPosition = new Vector2(0.3f, -0.28f);
        }
        else {
            transform.localPosition = new Vector2(0.3f, 0.12f);
        }

        
        
        //transform.position = parent.position;
        transform.rotation = parent.rotation;
        gameObject.tag = parent.tag;
        gameObject.SetActive(true);
        Rigidbody2D rb = GetComponent<Rigidbody2D>();

        Vector2 newAngle = new Vector2(Mathf.Cos(offset * (Mathf.PI/180)), Mathf.Sin(offset * (Mathf.PI / 180)));

        rb.AddRelativeForce(newAngle * speed * 100);
        GetComponent<Damager>().damage = damage;
        transform.SetParent(null);
    }

    /*private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == gameObject.tag)
        {
            return;
        }

        gameObject.SetActive(false);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == gameObject.tag)
        {
            return;
        }

        gameObject.SetActive(false);
    }*/
}
