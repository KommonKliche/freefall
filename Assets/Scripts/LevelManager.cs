﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct DebrisCall {
    [SerializeField]
    private GameObject specialDebris;
    [SerializeField]
    private float callTime;
    [SerializeField]
    private float frequency;
    [SerializeField]
    private float duration;

    public GameObject SpecialDebris {
        get { return specialDebris; }
    }

    public float CallTime{
        get { return callTime; }
    }

    public float Frequency {
        get { return frequency; }
    }

    public float Duration {
        get {
            return duration;
        }
    }

    public DebrisCall(float callTime, float frequency, float duration) {
        this.specialDebris = null;

        this.callTime = callTime;
        if (frequency >= 1)
        {
            this.frequency = frequency;
        }
        else {
            this.frequency = 1;
        }
        this.duration = duration;
    }

}

public class LevelManager : MonoBehaviour
{
    static protected LevelManager levelManager;
    static public LevelManager S {
        get { return levelManager; }
    }

    private Coroutine end;
    public float debrisRate = 5.0f;
    //public float debrisTimer = 0.0f;
    [SerializeField]
    private float dsSlow = 2.0f;
    [SerializeField]
    private float dsFast = 4.0f;
    [SerializeField]
    private float dsDrift = 0.25f;
    [SerializeField]
    private GameObject floor;
    [SerializeField]
    private GameObject debris;
    [SerializeField]
    protected List<GameObject> nodes;
    [SerializeField]
    private List<DebrisCall> debrisCalls;
    private List<Coroutine> callers;//callers of debris
    [SerializeField]
    protected float parachuteTime = 15;//seconds the player uses the parachute5
    private PlayerController player;
    public GameObject parachute;
    public bool parachuteReleased;
    public float currentTime = 0; //current height in meters
    
    public float currentSpeed = 200 * 1000.0f / 60.0f / 60.0f; // speed at which the player travels
    public float setSpeed = 10;
    // speeds in meters per second
    public float BellySpeed {
        get { return 200 * 1000.0f / 60.0f / 60.0f; }
    }

    public float HeadSpeed
    {
        get { return 240 * 1000.0f / 60.0f / 60.0f; }
    }

    public float ParachuteSpeed
    {
        get { return 28 * 1000.0f / 60.0f / 60.0f; }
    }

    public List<GameObject> Nodes {
        get { return nodes; }
    }

    // initial height in meters
    [SerializeField]
    private float levelTime = 30; //level's length in seconds
    public float LevelTime
    {
        get { return levelTime; }
    }

    private void Awake()
    {
        if (levelManager == null)
        {
            levelManager = this;
        }
        else {
            Destroy(this.gameObject);
        }

        player = FindObjectOfType<PlayerController>();



    }

    // Start is called before the first frame update
    void Start()
    {
        //debrisTimer = 0.0f;
        currentTime = 0;
        currentSpeed = setSpeed;
        parachute = null;
        parachuteReleased = false;

        callers = new List<Coroutine>();

        foreach (DebrisCall d in debrisCalls) {

            callers.Add(StartCoroutine(debrisCaller(d)));
        }

    }

    private void FixedUpdate()
    {
        if (currentTime >= 5) {
            //debrisTimer += Time.deltaTime;
        }
        
        if (player == null) {
            if (end == null)
            {
                end = StartCoroutine(EndBad());
            }
        }

        if (currentTime < levelTime && player != null)
        {
            currentTime += Time.deltaTime;

            if (currentTime > levelTime - parachuteTime  &&  parachute == null && !parachuteReleased) {
                PlayerController player = GameObject.FindObjectOfType<PlayerController>();
                parachute = GameObject.Instantiate(player.parachutePrefab, player.gameObject.transform);
                parachuteReleased = true;
            }
        }
        else {
            if (end == null && player != null) {
                end = StartCoroutine(End());
            }
        }
    }

    protected void Spawning() {
        if (nodes.Count < 10) {
            Debug.Log("Not enough nodes");
            return;
        }
    }

    private IEnumerator EndBad() {
        Results.S.gameObject.SetActive(true);
        Results.S.ShowResults();
        //Deal Land damage
        //Load Up new Scene
        yield break;
    }

    private IEnumerator End() {
        //Take Away Control from player
        //Check landing speed
        player.controllable = false;

        while (floor.transform.position.y < -3.0)
        {
            floor.transform.position = new Vector2(0.0f, floor.transform.position.y + (currentSpeed * Time.deltaTime));
            yield return new WaitForSeconds(1/60.0f);
        }
        floor.transform.position = new Vector2(0, -3.0f);

        if (FindObjectOfType<ParticleControl>() != null) {
            FindObjectOfType<ParticleControl>().cloudSystem.Clear();
        }

        while (player.transform.position.y > -2.5f) {
            player.transform.position = new Vector2(player.transform.position.x, player.transform.position.y - (currentSpeed * Time.deltaTime));
            yield return new WaitForSeconds(1 / 60.0f);
        }
        player.transform.position = new Vector2(player.transform.position.x, -2.5f);

        bool parachuteOn = false;

        if (parachute != null) {
            parachuteOn = true;
        }

        player.GetComponent<PlayerController>().End(parachuteOn);

        if (parachute != null)
        {
            Debug.Log("Landing OK");
            //ok
            //Disable Parachute
            parachute.SetActive(false);
        }
        else
        {
            Debug.Log("Landing Damage");
            player.GetComponent<Damageable>().TakeDamage(3);
            //take 3 damage
        }

        currentSpeed = 0;

        Results.S.gameObject.SetActive(true);
        Results.S.ShowResults();
        //Deal Land damage
        //Load Up new Scene
        yield break;
    }

    public void CreateDebris(GameObject debPrefab) {
        int[] nodesUsed = { -1, -1, -1} ;

        if (debPrefab == null) {
            debPrefab = debris;
        }

        if (nodes.Count < 10)
        {
            Debug.Log("Not enough nodes");
            return;
        }

        int debrisCurve = (int)(currentTime / levelTime * 2);

        int numDebris = Random.Range(1, 2 + debrisCurve);

        //instantiate debris based on num debris
        for (int i = 0; i < numDebris; i++) {
            int useNode = Random.Range(0,10);

            //If the node is already being used, choose another node
            while (useNode == nodesUsed[0] || useNode == nodesUsed[1] || useNode == nodesUsed[2]) {
                useNode = Random.Range(0, 10);
            }

            nodesUsed[i] = useNode;


            GameObject d = Instantiate(debPrefab);
            d.transform.position = nodes[useNode].transform.position;

            //Set speed of debris
            if (useNode < 5)
            {
                d.GetComponent<Rigidbody2D>().velocity = new Vector2( Random.Range(-dsDrift, dsDrift), Random.Range(dsSlow, dsFast) * -1.0f);
            }
            else {
                d.GetComponent<Rigidbody2D>().velocity = new Vector2( Random.Range(-dsDrift, dsDrift), Random.Range(dsSlow, dsFast));
            }
        }

    }

    public IEnumerator debrisCaller(DebrisCall debrisCall) {
        float durationTimer = 0.0f;
        float debrisTimer = 0.0f;
        while (currentTime < levelTime - 5 && durationTimer < debrisCall.Duration) {
            durationTimer += (1/10.0f);
            debrisTimer += (1 / 10.0f);
            if (currentTime >= debrisCall.CallTime) {
                if (debrisTimer >= debrisCall.Frequency)
                {
                    debrisTimer = 0.0f;
                    CreateDebris(debrisCall.SpecialDebris);
                }
            }
            yield return new WaitForSeconds(1/10.0f);
        }
        yield break;
    }

}
