﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public static class SaveSystem
{
    public static void SaveScore(int score, string name) {
        BinaryFormatter formatter = new BinaryFormatter();
        string path = Application.persistentDataPath + "/freefallDataScr.fun";

        FileStream stream = new FileStream(path, FileMode.Create);

        HighscoreData highscore = new HighscoreData(score, name);

        formatter.Serialize(stream, highscore);
        Debug.Log("highscore captutred: " + score + "/" + name);
        stream.Close();
    }

    public static HighscoreData highscoreData() {
        string path = Application.persistentDataPath + "/freefallDataScr.fun";
        if (File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);

            if (stream.Length == 0) {
                stream.Close();
                SaveScore (50, "LNF");
                return new HighscoreData(50, "LNF");
            }

            HighscoreData highscore = formatter.Deserialize(stream) as HighscoreData;

            stream.Close();
            return highscore;
        }
        else {
            Debug.LogError("Save data not found in: " + path);
            SaveScore(50, "LNF");
            return new HighscoreData(50, "LNF");
        }
    }

}
