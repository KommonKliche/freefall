﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunPickup : MonoBehaviour
{
    [SerializeField]
    private Gun gun; // place gun in here

    private void OnTriggerEnter2D(Collider2D other)
    {

        if (other.gameObject.tag == "Player" && gun != null)
        {
            PlayerController player = other.gameObject.GetComponent<PlayerController>();

            if (player != null) {
                player.EquipGun(gun);
                Debug.Log("Gun Change Attempt");
                Destroy(gameObject);

            }
        }
    }
}
