﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreKeeper : MonoBehaviour
{
    public static ScoreKeeper S; // Scorekeeper Singleton

    private int enemiesInLevel = 0;

    private int hits = 0;

    public int Hits {
        get { return hits; }
    }

    private int misses = 0;

    public int Misses {
        get { return misses; }
    }

    public int Results {
        get {
            int totalScore = 100;

            int percentPerEnemy = 0;

            if (enemiesInLevel > 0) {
                percentPerEnemy = 100 / enemiesInLevel;
            }

            totalScore = 100 - (percentPerEnemy * misses);
            return totalScore;
        }
    }

    // Start is called before the first frame update
    void Awake()
    {
        if (S == null)
        {
            S = this;
        }
        else {
            Destroy(this.gameObject);
        }
        enemiesInLevel = 0;
        hits = 0;
        misses = 0;
    }

    public void AddEnemy() {
        enemiesInLevel++;
    }

    public void AddMiss() {
        misses++;
    }

    public void AddHits() {
        hits++;
    }

}
