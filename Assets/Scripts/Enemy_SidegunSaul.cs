﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_SidegunSaul : Enemy
{
    protected override void Aim()
    {
        //Shoot Straight
        if (GameObject.FindGameObjectWithTag("Player") == null || !arm.gameObject.activeInHierarchy)
        {
            return;
        }

        GameObject player = GameObject.FindGameObjectWithTag("Player");

        if (player.transform.position.x > transform.position.x)
        {
            //aim right
            arm.transform.eulerAngles = new Vector3(0, 0, -90);
        }
        else
        {
            //aim left
            arm.transform.eulerAngles = new Vector3(0, 0, 90);
        }
    }

    override protected IEnumerator EnemyBehavior()
    {

        //has enemy wait
        while (!added)
        {
            if ((int)LevelManager.S.currentTime >= releaseTime + 5)
            {
                /*if (ScoreKeeper.S != null)
                {
                    ScoreKeeper.S.AddEnemy();
                }*/
                added = true;
                Activate(true);
            }
            yield return new WaitForSeconds(1 / 60.0f);
        }

        for (int i = 0; i < shootingRate; i++)
        {
            if (GameObject.FindGameObjectWithTag("Player") == null)
            {
                //EXIT

                enemyBehavior = StartCoroutine(Exit());
                yield break;
            }

            limitY = true;

            while (Mathf.Abs(GameObject.FindGameObjectWithTag("Player").transform.position.y - transform.position.y) > 0.5f)
            {
                if (GameObject.FindGameObjectWithTag("Player").transform.position.y < transform.position.y)
                {
                    rb.AddForce(new Vector2(0, -movementSpeed));
                }
                else
                {
                    rb.AddForce(new Vector2(0, movementSpeed));
                }
                yield return new WaitForSeconds(1 / 60.0f);
            }

            yield return new WaitForSeconds(0.33f);

            if (GameObject.FindGameObjectWithTag("Player") == null)
            {
                //EXIT
                enemyBehavior = StartCoroutine(Exit());
                yield break;
            }

            Debug.Log("Enemy Shooting"); //Insert Shoot Here
            gun.GetComponent<Gun>().Shoot();
            yield return new WaitForSeconds(0.15f);

        }

        enemyBehavior = StartCoroutine(Exit());
        yield break;
    }
}
