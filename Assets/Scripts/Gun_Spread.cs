﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun_Spread : Gun
{
    [SerializeField]
    private float spreadAngle = 90;// 90 degree cone ahead

    [SerializeField]
    private int spreadShots = 2; // shots in the spread

    private int SpreadShots {
        get {
            if (spreadShots < 2) {
                return 2;
            }
            if (spreadShots > maxBullets) {
                return maxBullets;
            }
            return spreadShots;
        }
    }


    public override void Shoot()
    {
        if (Time.time >= nextShot)
        {
            int shotsFired = 0; //shots fired in spread
            float angleSet = spreadAngle / 2.0f;

            if (shotSound != null) {
                shotSound.Play();
            }

            foreach (GameObject bull in bullets) // Search through all of the bullets
            {
                if (bull.activeSelf == false) // If it hasn't been fired yet...
                {
                    float offset = angleSet -  (spreadAngle * ( (float)shotsFired / (float)(SpreadShots - 1))) ;

                    bull.GetComponent<Bullet>().Fire(bulletSpeed, bulletDamage, transform, offset, GetComponentInChildren<SpriteRenderer>().flipY); // Call the bullet's Fire method

                    shotsFired++;

                    if (shotsFired >= SpreadShots) {
                        Debug.Log("spread");
                        nextShot = Time.time + (1 / fireRate); // Set the timer for the next shot
                        break; // Leave the loop until the next time the player shoots
                    }
                }
            }
        }
    }





}
