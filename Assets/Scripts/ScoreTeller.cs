﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreTeller : MonoBehaviour
{
    public Text text;

    public void Start()
    {
        if (ScoreKeeper.S == null) {
            Debug.Log("ScoreTeller: Scorekeeper missing");
        }
    }
    public void Update()
    {
        if (ScoreKeeper.S == null) {
            return;
        }
        text.text = "Hits:\t\t" + ScoreKeeper.S.Hits + "\nMiss:\t" + ScoreKeeper.S.Misses;
    }

}
