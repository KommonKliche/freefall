﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LootDropper : MonoBehaviour
{
    [Tooltip("Integer between 0 and 10. Setting to 5 is equivalent to 50%")]
    [SerializeField]
    private int dropRate = 1;//Chance of dropping out of 100

    public GameObject dropItemPrefab;

    public void rollDrop()
    {
        int roll = Random.Range(0, 10);
        Debug.Log("Dropper rolled " + roll);

        if (roll < dropRate && dropItemPrefab != null)
        {
            GameObject drop = Instantiate(dropItemPrefab);
            drop.transform.position = transform.position;
            drop.GetComponent<Rigidbody2D>().velocity = GetComponent<Rigidbody2D>().velocity;
        }
        
    }
}
