﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_DaringDarline : Enemy
{
    private float activeTime = 0.0f;
    [SerializeField]
    private float chaseTime = 7.0f;
    [SerializeField]
    private int shootFrequency = 2;
    [SerializeField]
    private float followRadius = 3;


    protected override void Aim()
    {
        //Shoot Straight
        if (GameObject.FindGameObjectWithTag("Player") == null || !arm.gameObject.activeInHierarchy)
        {
            return;
        }

        timeToShoot += Time.deltaTime; //set Up to shoot at frequency 

        GameObject player = GameObject.FindGameObjectWithTag("Player");

        float shotAngle = -90;

        //Get hypotenuse
        float measureDistance = Mathf.Sqrt(Mathf.Pow((player.transform.position.y - transform.position.y), 2) + Mathf.Pow((player.transform.position.x - transform.position.x), 2));

        //calculate 
        shotAngle += (Mathf.Asin((player.transform.position.y - transform.position.y) / measureDistance) / (2 * Mathf.PI)) * 360.0f;

        if (GetComponent<SpriteRenderer>().flipX)
        {
            shotAngle += 180 - ((Mathf.Asin((player.transform.position.y - transform.position.y) / measureDistance) / (2 * Mathf.PI)) * 360.0f) * 2.0f;
        }

        arm.transform.eulerAngles = new Vector3(0, 0, shotAngle);

        if (timeToShoot >= shootingRate)
        {
            gun.GetComponent<Gun>().Shoot();
            timeToShoot = 0.0f; // reset timer to shoot
        }
    }

    protected override IEnumerator EnemyBehavior() {
        while (!added)
        {
            if ((int)LevelManager.S.currentTime >= releaseTime + 5)
            {
                /*if (ScoreKeeper.S != null)
                {
                    ScoreKeeper.S.AddEnemy();
                }*/
                added = true;
                Activate(true);
            }
            yield return new WaitForSeconds(1 / 60.0f);
        }

        transform.position = new Vector2(transform.position.x, 6);

        while (transform.position.y > -6) {
            rb.AddForce(new Vector2(0.0f, movementSpeed * -2));
            yield return new WaitForSeconds(1 / 60.0f);
        }

        while (transform.position.y < -5) {
            rb.AddForce(new Vector2(0.0f, movementSpeed * 2));
            yield return new WaitForSeconds(1 / 60.0f);
        }
        activeTime = 0;
        float shootTime = chaseTime / (shootFrequency + 1);
        int timesShot = 0;

        limitY = true;

        while (activeTime < chaseTime) {
            if (GameObject.FindGameObjectWithTag("Player") == null)
            {
                //EXIT
                enemyBehavior = StartCoroutine(Exit());
                yield break;
            }

            Vector2 playerPosition = GameObject.FindGameObjectWithTag("Player").transform.position;

            //Control X Position
            if (playerPosition.x + Mathf.Cos(activeTime) * Random.Range(-1,1) * followRadius  > transform.position.x)
            {
                    rb.AddForce(new Vector2(movementSpeed, 0));
            }
            else
            {
                    rb.AddForce(new Vector2(-movementSpeed, 0));
            }

            //Control Y Position
            if (playerPosition.y + Mathf.Sin(activeTime) * Random.Range(-1, 1) * followRadius > transform.position.y)
            {
                rb.AddForce(new Vector2(0, movementSpeed));
            }
            else
            {
                rb.AddForce(new Vector2(0, -movementSpeed));
            }

            //control shooting
            if (activeTime >= shootTime * (timesShot + 1) && timesShot < shootFrequency) {
                //Shoot
                

                Debug.Log("Darline Shoots");
                timesShot += 1;
            }
            yield return new WaitForSeconds(1 / 60.0f);
            activeTime += (1 / 60.0f);
        }

        enemyBehavior = StartCoroutine(Exit());
        yield break;
    }
}
