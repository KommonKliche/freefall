﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleControl : MonoBehaviour
{
    public ParticleSystem cloudSystem;
    [SerializeField]
    private float slowSpeed = 0.25f;
    [SerializeField]
    private float normalSpeed = 1.0f;
    // Start is called before the first frame update
    void Start()
    {
        cloudSystem = GetComponent<ParticleSystem>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        var main = cloudSystem.main;

        if (LevelManager.S.currentTime >= LevelManager.S.LevelTime - 5) {

            if (main.simulationSpeed > 0.15f)
            {
                main.simulationSpeed -= Time.deltaTime/4; // stop simulation
            }
            else {
                if (LevelManager.S.currentTime >= LevelManager.S.LevelTime - 1) {
                    main.simulationSpeed = 0.0001f; // stop simulation
                }   
            }
            
        }
        else if (LevelManager.S.parachute != null)
        {
            main.simulationSpeed = slowSpeed; // slow sim
        }
        else {
            main.simulationSpeed = normalSpeed; // norm sim
        }

    }
}
