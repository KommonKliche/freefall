﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Decoration : MonoBehaviour
{
    private Rigidbody2D rb;
    private float timeCalc;
    private Vector2 startPosition;
    [SerializeField]
    private float distance = 1;
    [SerializeField]
    private float movementSpeed = 1;

    // Start is called before the first frame update
    void Start()
    {
        startPosition = transform.position;
        rb = GetComponent<Rigidbody2D>();
        timeCalc = 0;
    }

    // Update is called once per frame
    void Update()
    {
        timeCalc += Time.deltaTime;

        if ( startPosition.x + Mathf.Cos(timeCalc) * Random.Range(-0.5f, 1) * distance > transform.position.x)
        {
            rb.AddForce(new Vector2(movementSpeed, 0));
        }
        else
        {
            rb.AddForce(new Vector2(-movementSpeed, 0));
        }

        //Control Y Position
        if (startPosition.y + Mathf.Sin(timeCalc) * Random.Range(-0.5f, 1) * distance > transform.position.y)
        {
            rb.AddForce(new Vector2(0, movementSpeed));
        }
        else
        {
            rb.AddForce(new Vector2(0, -movementSpeed));
        }
    }
}
